import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class InfosScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
           
            <Text>Name:		Liezel Butulan Alquiza</Text>
            <Text>Age:		20</Text>
            <Text>Birthday:	January 14, 1998</Text>
            <Text>Email:	liezel10175@gmail.com</Text>
            <Text>School:	University of SouthEastern Philippines</Text>
            <Text>Course:	Bachelor of Science in Information Technology</Text>
            <Text>Section:	BSIT-3B</Text>
            <Button
              title="Home"
              onPress={() => this.props.navigation.navigate('Home')}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

const fontSize = StyleSheet.create({
    container: {
      fontSize: 20, 
    },
  });


export default InfosScreen;
